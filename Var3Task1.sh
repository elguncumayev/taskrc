printf "Select a number between 5-12 (inclusive):"
read input_number;
while [[ $input_number != +([0-9]) ]] || (( input_number < 5 || input_number > 12 ));
do
    printf "The input you entered isn't between 5-12\n"
    printf "Please select another number: "
    read input_number; 
done 
printf "All done!"