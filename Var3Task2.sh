#! /bin/bash
while :
do
    echo "Enter file name (if outside of this directory with full path): "
    read inputFileName

    if [ -f $inputFileName ]
    then
        cat $inputFileName
        break
    else
        echo "File does not exist. Try again."
    fi
done